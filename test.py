import unittest
from main import calc
 
class test_case(unittest.TestCase):
 
    def test_sum(self):
        calculation = calc(8, 2)
        self.assertEqual(calculation.get_sum(), 10, 'The sum is wrong.')
 
    def test_diff(self):
        calculation = calc(8, 2)
        self.assertEqual(calculation.get_diff(), 6, 'The diffrence is wrong.')

    def test_prod(self):
        calculation = calc(8, 2)
        self.assertEqual(calculation.get_prod(), 16, 'The product is wrong.')


if __name__ == '__main__':
    unittest.main()